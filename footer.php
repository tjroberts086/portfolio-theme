

      </div>
      <div class="container-fluid footer text-center blog-footer">


          <a href="#"><i class="fa fa-facebook-official fa-2x"></i></a>
          <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
          <a href="#"><i class="fa fa-linkedin fa-2x"></i></a>

          <p>
              Copyright 2017 grimtv.com
          </p>

          <p>
              <a href="#">Back to top</a>
          </p>
      </div>


    <!-- /.container -->
    <?php wp_footer(); ?>
    <!-- jQuery -->
    <script src="<?php bloginfo('template_directory');?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php bloginfo('template_directory');?>/js/bootstrap.min.js"></script>

</body>
<?php /* Template Name: Series Template */ ?>

<?php get_header(); ?>

<!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Series
        </h1>
    </div>
</div>

<?php
$post_num = 0;
query_posts('category_name=series'); while ( have_posts() ) : the_post();               if($post_num == 0) {
    echo '<div class="row">';
}

    $post_num++;

    get_template_part( 'grid-content-2', get_post_format() );

    if($post_num ==3) {
        echo '</div>';
        $post_num = 0;
    }

endwhile;
?>

<!-- /.row -->


<?php get_footer(); ?>
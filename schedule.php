<?php /* Template Name: Schedule Template */ ?>

<?php get_header(); ?>

<!-- Page Header -->


        <h1 class="page-header">Schedule
        </h1>

<div class="row">
    <div class="col-lg-2 hr">
        <h1>1:00PM</h1>
    </div>
    <div class="col-lg-10 bot-border">
        <h1>Nightmare on Elm Street</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <h1>3:00PM</h1>
    </div>
    <div class="col-lg-10">
        <h1>Silence of the Lambs</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <h1>5:00PM</h1>
    </div>
    <div class="col-lg-10">
        <h1>People Under the Stairs</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <h1>7:00PM</h1>
    </div>
    <div class="col-lg-10">
        <h1>Childs Play</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <h1>9:00PM</h1>
    </div>
    <div class="col-lg-10">
        <h1>The Lost Boys</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <h1>11:00PM</h1>
    </div>
    <div class="col-lg-10">
        <h1>Friday the 13th</h1>
    </div>
</div>

<!-- /.row -->


<?php get_footer(); ?>
<a href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail('medium_large',
        array('class' => 'fill')); ?>
</a>
<div class="carousel-caption">
    <h2>The Lost Boys</h2>
</div>
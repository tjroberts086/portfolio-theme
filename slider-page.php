<?php /* Template Name: Slider Template */ ?>


<?php get_header(); ?>

<!-- Page Header -->

<body>
<div id="wrapper">

    <!-- Page Content -->

<!-- Full Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <?php
            $post_num = 0;
            query_posts('category_name=Featured'); while ( have_posts() && $post_num < 3 ) : the_post();
                if($post_num == 0) {
                    echo '<div class="item active">';
                } else {
                    echo '<div class="item">';
                }

                $post_num++;

                get_template_part( 'slider-content', get_post_format() );

                echo '</div>';

            endwhile;
            ?>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </header>


<?php get_footer(); ?>